﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiCommon
{
    /// <summary>
    /// Json Web Key that assumes usage of RS256 algo. The algo is one of the inputs when using Jose JWT library.
    /// </summary>
    public class JWKSItem
    {
        /// <summary>
        /// Exponent
        /// </summary>
        public String e { get; set; }

        /// <summary>
        /// Modulus
        /// </summary>
        public String n { get; set; }
        public String kty { get; set; }
        public String use { get; set; } //Optional parameter either sig or enc.
        public String alg { get; set; }

        public String kid { get; set; }
        public JWKSItem(String exponent, String modulus)
        {
            alg = "RS256";
            kty = "RSA";
            e = exponent;
            n = modulus;
            use = "sig"; //sig means that the key is NOT used for encryption
        }

        //Empty constructor required for serialization
        public JWKSItem()
        {

        }
    }
}
