﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiCommon
{
    public class EnvironmentHelper
    {
        public static String GetEnvVariable(String key)
        {
            var result = Environment.GetEnvironmentVariable(key, EnvironmentVariableTarget.Process);
            return result;
        }
    }
}
