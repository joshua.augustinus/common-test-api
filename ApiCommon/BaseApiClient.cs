﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiCommon
{
    public class BaseApiClient
    {
        public async Task<T> DeserializeAsync<T>(HttpResponseMessage response)
        {

            response.EnsureSuccessStatusCode();
            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            };

            using (var responseStream = await response.Content.ReadAsStreamAsync())
            {
                var result = await System.Text.Json.JsonSerializer.DeserializeAsync<T>(responseStream, options);
                return result;
            }

        }
    }
}
