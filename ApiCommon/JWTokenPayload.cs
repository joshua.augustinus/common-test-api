﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiCommon
{
    public class JWTokenPayload
    {
        public String sub { get; set; }
        public String exp { get; set; }
        public String iss { get; set; }

        public String kid { get; set; }
    }
}
