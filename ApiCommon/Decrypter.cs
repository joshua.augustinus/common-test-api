﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace ApiCommon
{
    /// <summary>
    /// There is a potential race condition here. The Decrypter needs to get the public key first before
    /// it can decode tokens. So the API wants to instantiate Decrypter well before decoding anything
    /// </summary>
    public class Decrypter
    {
        IPublicKeyProvider Provider { get; set; }
        private String PUBLIC_KEYPATH = $"App_Data/publickey.xml";
        public Decrypter(IPublicKeyProvider provider)
        {
            Provider = provider;
        }

        /**
         * This method will throw exception if the token is not valid
         */
        public  JWTokenPayload DecodeToken(String token )
        {
            var publicKey = Provider.GetPublicKey(token);
            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
            {
                var parameters = new RSAParameters();
                parameters.Modulus = Convert.FromBase64String(publicKey.n);
                parameters.Exponent = Convert.FromBase64String(publicKey.e);
                rsa.ImportParameters(parameters);
                // This will throw if the signature is invalid
                return Jose.JWT.Decode<JWTokenPayload>(token, rsa, Jose.JwsAlgorithm.RS256);
            }
        }

        public  string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }


    }
}
