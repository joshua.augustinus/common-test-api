﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiCommon
{
    public class PublicKeyProvider : BaseApiClient,  IPublicKeyProvider
    {
        Dictionary<String, JsonWebKeySet> PublicKeys { get; set; }
        public PublicKeyProvider()
        {
            PublicKeys = new Dictionary<String, JsonWebKeySet>();
           
        }

        public int GetNumberOfKeys()
        {
            return PublicKeys.Count();
        }

        public async Task ParseDiscoveryEndpoints(IEnumerable<String> endpoints)
        {
            foreach(var endpoint in endpoints)
            {
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(endpoint);

                    var result = await DeserializeAsync<DiscoveryDocument>(response);

                    await AddJsonWebKeySet(result.issuer, result.jwks_uri);
                }
            }
        }

        public async Task AddJsonWebKeySet(String issuer, String jwksEndpoint)
        {
 
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(jwksEndpoint);

                    var result = await DeserializeAsync<JsonWebKeySet>(response);

                    PublicKeys.Add(issuer, result);
                }
            

        }

        /**
         * Finds the issuer from an accesstoken
         * Then finds the appropriate public key that was already saved
         * Assumes that ParseDiscoveryEndpoints was already called to fetch public keys
         */
        public JWKSItem GetPublicKey(String accessToken)
        {
            if (PublicKeys.Count == 0)
            {
                throw new ApplicationException("You need to call AddJsonWebKeySet or ParseDiscoveryEndpoints first");
            }

            //Find the issuer and kid
            var split = accessToken.Split('.');
            var payloadString = Base64Decode(split[1]);
            //Get payload
            var payload = JsonConvert.DeserializeObject<JWTokenPayload>(payloadString);
            var iss = payload.iss;

            if (String.IsNullOrEmpty(iss))
            {
                return PublicKeys.First().Value.keys.First();
            }
            else
            {
               var keySet = PublicKeys.Where(x => x.Key == iss).First().Value;

                //now find the one with matching kid
                var kid = payload.kid;

                if (String.IsNullOrEmpty(kid))
                {
                    return keySet.keys.First();
                }
                else
                {
                    return keySet.keys.Where(x => x.kid == kid).First();
                }
            }
        }

        private String Base64Decode(String base64)
        {
            var base64Encoded = base64.PadRight(base64.Length + (4 - base64.Length % 4) % 4, '=');
            byte[] data = System.Convert.FromBase64String(base64Encoded);
            return System.Text.ASCIIEncoding.ASCII.GetString(data);
        }

 
    }

    public class PublicKeyResponse
    {
        public String PublicKey { get; set; }
    }
}
