﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiCommon
{
    public class JsonWebKeySet
    {
        public JWKSItem[] keys { get; set; }
    }
}
