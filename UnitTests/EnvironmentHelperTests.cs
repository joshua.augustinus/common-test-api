﻿using ApiCommon;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace UnitTests
{
    public class EnvironmentHelperTests
    {
        [Fact]
        public void CanGetEnvironmentVariable()
        {
            var result = EnvironmentHelper.GetEnvVariable("PERSONAL_MEDICARE_NAME");
            Assert.Equal("Joshua Augustinus",result);
        }
    }
}
